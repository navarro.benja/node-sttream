# Dockerfile

FROM node:16
# Create app directory
RUN mkdir -p /opt/app
WORKDIR /opt/app
#RUN adduser app
#RUN chown -R app /opt/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install ci --only=production

# Bundle app source
COPY . .
#USER app
EXPOSE 8080
CMD [ "npm", "run", "pm2" ]
