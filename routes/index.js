const express = require('express');
const router = express.Router();

// Filesystem
const fs = require('fs');
const StreamArray = require('stream-json/streamers/StreamArray');
const jsonStream = StreamArray.withParser();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Un ejemplo - Benjamin Navarro' });
});

router.get('/start', (req, res) => {
  let counter = 0;
  const readStream = fs.createReadStream('./data.json', { highWaterMark: 16 });
  const db = require('../database');

  readStream.pipe(jsonStream);

  jsonStream.on('data', ({ key, value }) => {

    db.Data.create({
      name: value.name,
      color: value.color || "default color",
      price: value.price || 0
    })
    .catch(err => {
      readStream.destroy();
      res.render('index', {
        title: 'Algo ha salido mal',
        result: counter,
        error: JSON.stringify(err)
      })
    });
    console.log(key, value);
    counter ++;
  });

  jsonStream.on('end', () => {
    console.log('Trabajo terminado...');
    res.render('index', {
      title: 'Resultados del ejemplo',
      result: counter
    });
  });
});

module.exports = router;