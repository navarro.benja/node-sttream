const express = require('express');
const router = express.Router();
const db = require('../database');

router.get("/all", function (req, res) {
  db.Data.findAll()
    .then(Datas => {
      res.status(200).send(JSON.stringify(Datas));
    })
    .catch(err => {
      res.status(500).send(JSON.stringify(err));
    });
});

router.get("/:id", function (req, res) {
  db.Data.findByPk(req.params.id)
    .then(Data => {
      res.status(200).send(JSON.stringify(Data));
    })
    .catch(err => {
      res.status(500).send(JSON.stringify(err));
    });
});

router.put("/", function (req, res) {
  db.Data.create({
    name: req.body.name,
    color: req.body.color,
    price: req.body.price
  })
    .then(Data => {
      res.status(200).send(JSON.stringify(Data));
    })
    .catch(err => {
      res.status(500).send(JSON.stringify(err));
    });
});

router.delete("/:id", function (req, res) {
  db.Data.destroy({
    where: {
      id: req.params.id
    }
  })
    .then(() => {
      res.status(200).send();
    })
    .catch(err => {
      res.status(500).send(JSON.stringify(err));
    });
});

module.exports = router;