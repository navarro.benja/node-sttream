# Simple app para introducir largos Objetos en DB

Utilizamos el paquete stream-json y sequelize. El primero para secuenciar los objetos que nos llegan por el stream y el segundo para la DB postgres

`npm install sequelize stream-json`

Rutas:
- `/start` comienza el traspaso de datos del json a la db
- `/all` Devuelve un array con todos los registros
- `/{id}` Devuelve un json con el id del registro, si está.

